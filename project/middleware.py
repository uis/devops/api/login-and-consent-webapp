from django.shortcuts import render
from social_django.middleware import SocialAuthExceptionMiddleware
from social_core.exceptions import SocialAuthBaseException
from social_core.utils import social_logger


class RenderSocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):
    """
    Middleware that handles Social Auth AuthExceptions providing a 400 response
    to the user.

    """

    def process_exception(self, request, exception):
        # Catch all social auth exceptions
        if isinstance(exception, SocialAuthBaseException):
            social_logger.error(exception)
            return render(request, '400.html', status=400)
