import os
import sys

import externalsettings

# By default, make use of connection pooling for the default database and use the Postgres engine.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'CONN_MAX_AGE': 60,  # seconds
    },
}

# By default we have no scope definitions. This setting should be populated from outside with a
# list which takes the following form:
#
# [
#     {
#         "title"       : "..."
#         "description" : "..."
#
#         "scopes" : {
#             "some-scope"       : "description"
#             "some-other-scope" : "description"
#         }
#     },
#
#     # ...
# ]
SCOPE_DESCRIPTIONS = []

CLOUDRUN_SERVICE_URL = ""

# If the EXTRA_SETTINGS_URLS environment variable is set, it is a comma-separated list of URLs from
# which to fetch additional settings as YAML-formatted documents. The documents should be
# dictionaries and top-level keys are imported into this module's global values.
_external_setting_urls = []
_external_setting_urls_list = os.environ.get('EXTRA_SETTINGS_URLS', '').strip()
if _external_setting_urls_list != '':
    _external_setting_urls.extend(_external_setting_urls_list.split(','))

externalsettings.load_external_settings(
    globals(), urls=_external_setting_urls,
    required_settings=[
        'SECRET_KEY', 'DATABASES',
        'SOCIAL_AUTH_GOOGLE_OAUTH2_KEY', 'SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET',
        'GATEWAY_OPENID_CONFIGURATION_URL', 'GATEWAY_BASE_URL',
        'GATEWAY_CLIENT_ID', 'GATEWAY_CLIENT_SECRET', 'GATEWAY_JWT_AUDIENCE',
        'UCAMLOOKUP_USERNAME', 'UCAMLOOKUP_PASSWORD',
    ],
    optional_settings=[
        'EMAIL_HOST', 'EMAIL_HOST_PASSWORD', 'EMAIL_HOST_USER', 'EMAIL_PORT',
        'GATEWAY_LOGIN_SCOPE', 'SCOPE_DESCRIPTIONS', 'CLOUDRUN_SERVICE_URL'
    ],
)

#: Base directory containing the project. Build paths inside the project via
#: ``os.path.join(BASE_DIR, ...)``.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

#: SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Be secure when running in GCP. CSRF_TRUSTED_ORIGINS needed to resolve host/origin header
# mismatch due to domain mappings in GCP.
if CLOUDRUN_SERVICE_URL:
    CSRF_TRUSTED_ORIGINS = [CLOUDRUN_SERVICE_URL]
    SECURE_SSL_REDIRECT = True

# Being more permissive than general GCP recommendations for ALLOWED_HOSTS as uptime checks
# may come to both CLOUDRUN_SERVICE_URL and the ".run.app" mapping.
ALLOWED_HOSTS = ['*']

#: Installed applications
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'whitenoise.runserver_nostatic',  # use whitenoise even in development
    'django.contrib.staticfiles',

    'automationcommon',
    'social_django',

    'login',
]

#: Installed middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'project.middleware.RenderSocialAuthExceptionMiddleware',
]

#: Login configuration
SOCIAL_AUTH_URL_NAMESPACE = 'social'
SOCIAL_AUTH_JSONFIELD_ENABLED = True

# Note: we use select_account here to support the "switch account" feature.
SOCIAL_AUTH_GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS = {'hd': 'cam.ac.uk', 'prompt': 'select_account'}
SOCIAL_AUTH_GOOGLE_OAUTH2_WHITELISTED_DOMAINS = ['cam.ac.uk']
SOCIAL_AUTH_GOOGLE_OAUTH2_WHITELISTED_HOSTED_DOMAINS = ['cam.ac.uk']

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'project.pipelines.enforce_hosted_domain',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

LOGIN_URL = '/accounts/login/google-oauth2/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

#: Root URL patterns
ROOT_URLCONF = 'project.urls'

#: Template loading
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        },
    },
]

#: WSGI
WSGI_APPLICATION = 'project.wsgi.application'


#: Password validation
#:
#: .. seealso:: https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


#: Internationalization
#:
#: .. seealso:: https://docs.djangoproject.com/en/2.0/topics/i18n/
LANGUAGE_CODE = 'en-gb'

#: Internationalization
TIME_ZONE = 'UTC'

#: Internationalization
USE_I18N = True

#: Internationalization
USE_L10N = True

#: Internationalization
USE_TZ = True

#: Static files (CSS, JavaScript, Images)
#:
#: .. seealso:: https://docs.djangoproject.com/en/2.0/howto/static-files/
STATIC_URL = '/static/'

#: Authentication backends
AUTHENTICATION_BACKENDS = [
    'social_core.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
]

STATIC_ROOT = os.environ.get('DJANGO_STATIC_ROOT', os.path.join(BASE_DIR, 'build', 'static'))

# By default we a) redirect all HTTP traffic to HTTPS, b) set the HSTS header to a maximum age
# of 1 year (as per the consensus recommendation from a quick Google search) and c) advertise that
# we are willing to be "preloaded" into Chrome and Firefox's internal list of HTTPS-only sites.
# Set the DANGEROUS_DISABLE_HTTPS_REDIRECT variable to any non-blank value to disable this.
if os.environ.get('DANGEROUS_DISABLE_HTTPS_REDIRECT', '') == '':
    # Exempt the healtch-check endpoint from the HTTP->HTTPS redirect.
    SECURE_REDIRECT_EXEMPT = ['^healthy/?$']

    SECURE_SSL_REDIRECT = True
    SECURE_HSTS_SECONDS = 31536000  # == 1 year
    SECURE_HSTS_PRELOAD = True
else:
    print('Warning: HTTP to HTTPS redirect has been disabled.', file=sys.stderr)

# We also support the X-Forwarded-Proto header to detect if we're behind a load balancer which does
# TLS termination for us. In future this setting might need to be moved to settings.docker or to be
# configured via an environment variable if we want to support a wider range of TLS terminating
# load balancers.
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


# Whether to support the x_forwarded_host in constructing the canonical url.
USE_X_FORWARDED_HOST = True

SWAGGER_SETTINGS = {
    # Describe token authentication in swagger definition
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header',
        },
    },
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Use cookie-based sessions for django to avoid races between sessions being written to the DB and
# redirects to the consent endpoint.
SESSION_ENGINE = "django.contrib.sessions.backends.signed_cookies"
SESSION_COOKIE_HTTPONLY = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# Required to preserve state when redirecting back from Google log in. "Lax" is the default value.
SESSION_COOKIE_SAMESITE = 'Lax'
