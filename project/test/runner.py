"""
Custom test suite runner.

"""
import django.test.runner


class BufferedDiscoverRunner(django.test.runner.DiscoverRunner):
    """
    A sub-class of :py:class:`django.test.runner.DiscoverRunner` which has
    exactly the same behaviour except that the *buffer* keyword argument to the constructor
    is forced to be True.

    The upshot of this is that output to stdout and stderror is captured and
    only reported on test failure.

    """
    def __init__(self, *args, **kwargs):
        kwargs['buffer'] = True
        return super().__init__(*args, **kwargs)
