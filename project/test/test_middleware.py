from unittest import mock

import faker
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from social_core.exceptions import AuthUnreachableProvider


class TestRenderSocialAuthExceptionMiddleware(TestCase):

    def setUp(self):
        test_faker = faker.Faker()
        # Create a fake @cam.ac.uk user and sign them in via Google.
        user = get_user_model().objects.create(
            username=test_faker.user_name(), email=f'{test_faker.user_name()}@cam.ac.uk')
        self.client.force_login(user, 'social_core.backends.google.GoogleOAuth2')

    @mock.patch('login.views.initiate', side_effect=AuthUnreachableProvider)
    def test_social_auth_exception_rendered(self, mock_initiate):
        with self.assertTemplateUsed('400.html'):
            self.client.get(reverse('login-initiate'), HTTP_HOST='login.example.invalid')
