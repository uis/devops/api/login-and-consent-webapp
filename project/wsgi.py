"""
WSGI config for Login and Consent.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from proxyprefix.wsgi import ReverseProxiedApp

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings")


application = ReverseProxiedApp(get_wsgi_application())
