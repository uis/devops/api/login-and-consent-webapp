from django.urls import path

from . import views

urlpatterns = [
    path('', views.initiate, name='login-initiate'),
    path('consent', views.consent, name='login-consent'),
]
