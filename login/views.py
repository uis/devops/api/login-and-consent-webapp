import json
import logging
import urllib.parse

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotAllowed, HttpResponse
from django.shortcuts import render
import ibisclient.connection
from ibisclient.methods import PersonMethods
from jwcrypto.jwt import JWTExpired
import requests

from . import gateway


LOG = logging.getLogger(__name__)


@login_required
def initiate(request):
    if request.method != 'GET':
        return HttpResponseNotAllowed(['GET'])

    # User *must* have an @cam.ac.uk address. Extract the CRSid.
    if request.user.email is None or not request.user.email.endswith('@cam.ac.uk'):
        LOG.error('Non cam.ac.uk user signed in: %r', request.user.email)
        return _render_403(request)
    crsid = request.user.email.split('@')[0]

    # Extract and verify the session token. Return 400 responses if the token is invalid or
    # missing. Note that we are passed the expected audience explicitly as a setting. It would be
    # tempting to simply use request.build_absolute_uri('/') here but that means we are trivially
    # vulnerable to a confused deputy attack since the Host header is potentially manipulable.
    unverified_token = request.GET.get('token')
    if unverified_token is None:
        LOG.info('No token present in initiate request.')
        return _render_400(request)
    try:
        session_id = gateway.decode_session_token(unverified_token, settings.GATEWAY_JWT_AUDIENCE)
    except JWTExpired:
        LOG.exception('Session token expired.')
        return render(request, 'login/sessiontimeout.html', status=400)
    except Exception:
        LOG.exception('Error verifying session token.')
        return _render_400(request)

    # Get the session from the gateway. If we get a 404, the session became invalid between us
    # verifying the token and getting here.
    try:
        login_session = gateway.get_session(session_id)
    except requests.HTTPError as e:
        if e.response is not None and e.response.status_code == 404:
            # Login session expired just after verification
            LOG.exception('Session is invalid')
            return render(request, 'login/sessiontimeout.html', status=400)
        LOG.exception('Unknown HTTP error getting session info.')
        return _render_500(request)

    # Form a dictionary mapping scopes to the corresponding entry in SCOPE_DESCRIPTIONS.
    scope_descriptions_by_scope = {}
    for desc in settings.SCOPE_DESCRIPTIONS:
        for scope in desc.get('scopes', []):
            scope_descriptions_by_scope[scope] = desc

    # Filter list of scopes to those known to the application. The openid scope is always allowed
    # implicitly.
    filtered_scopes = {
        scope for scope in login_session.req_scope.split(' ')
        if scope in scope_descriptions_by_scope or scope in {'openid', 'profile', 'email'}
    }

    # Always add the "profile" scope, even if not asked for since we always return at least the
    # CRSid in the "sub" claim.
    filtered_scopes.add('profile')

    # Make a map from service name to scopes requested for that service and a description of the
    # service. The final map looks like:
    #
    # {
    #   "lookup": {
    #     "info": { "name": "...", "description": "...", "help_url", "... " },
    #     "scopes": [
    #       { "description": "..." },
    #     ]
    #   }
    # }
    #
    # We start with the basic profile scope which is built in above.
    service_scopes = {
        'oauth2': {
            'info': {
                'name': 'User profile',
                'description': 'Basic information about you from your profile.',
            },
            'scopes': [
                {'description': 'know your full name and avatar image if set'},
            ],
        }
    }

    # Add email if required.
    if 'email' in filtered_scopes:
        service_scopes['oauth2']['scopes'].append({'description': 'know your @cam email address'})

    # Gather other scopes by service.
    for scope in sorted(filtered_scopes):
        # Special case, openid is always allowed and the email and profile scopes are addressed
        # above.
        if scope == 'openid' or scope == 'profile' or scope == 'email':
            continue
        scope_description = scope_descriptions_by_scope[scope]
        service_name = scope_description["title"]
        service_info = scope_description.get('description', '')
        scope_info = {'description': scope_description['scopes'][scope]}
        if service_name in service_scopes:
            service_scopes[service_name]['scopes'].append(scope_info)
        else:
            service_scopes[service_name] = {
                'info': {'name': service_name, 'description': service_info},
                'scopes': [scope_info],
            }

    # Process service_scopes into a flat list of consents sorted by the service key.
    consents = [service_scopes[name] for name in sorted(service_scopes.keys())]

    # User claims always include the subject.
    user_claims = {'sub': f'{crsid}@v1.person.identifiers.cam.ac.uk'}

    # If filtered scopes includes "email", add the user's email address.
    if "email" in filtered_scopes:
        user_claims['email'] = f'{crsid}@cam.ac.uk'
        user_claims['email_verified'] = True

    # Extract Lookup profile for user
    person_methods = PersonMethods(_get_lookup_connection())
    person = person_methods.getPerson('crsid', crsid, fetch=['displayName'])

    # Set profile from lookup data
    if 'profile' in filtered_scopes:
        user_claims['name'] = person.displayName

    # Construct a template context.
    template_context = {
        'login_session': login_session,
        'app_url': _base_url(login_session.redirect_uri),
        'consents': consents,
        'person': person,
        'crsid': crsid,
    }

    # Record session id, filtered scopes and user claims in session.
    request.session['session_id'] = session_id
    request.session['filtered_scopes'] = ' '.join(sorted(filtered_scopes))
    request.session['user_claims_json'] = json.dumps(user_claims)
    request.session.save()

    return render(request, 'login/consent.html', context=template_context)


@login_required
def consent(request):
    # Only allow POSTs to this endpoint.
    if request.method != 'POST':
        return HttpResponseNotAllowed(['POST'])

    # Extract information from session.
    session_id = request.session.get('session_id')
    if session_id is None:
        LOG.error('Session id missing from session')
        return _render_500(request)
    filtered_scopes = request.session.get('filtered_scopes')
    if filtered_scopes is None:
        LOG.error('Scopes missing from session')
        return _render_500(request)
    user_claims_json = request.session.get('user_claims_json')
    if user_claims_json is None:
        LOG.error('User claims missing from session')
        return _render_500(request)
    user_claims = json.loads(user_claims_json)

    # Login is not strictly required for this endpoint since we have already extracted user data
    # and placed it in the session. We do, however, check that the person signed in is the one we
    # expect in case there are shenanigans going on.
    if user_claims['sub'].endswith('@v1.person.identifiers.cam.ac.uk'):
        crsid = user_claims['sub'].split('@')[0]
        if request.user.email != f'{crsid}@cam.ac.uk':
            LOG.error('Mismatched user %r for claims: %r', request.user.email, user_claims)
            return _render_400(request)

    if request.POST.get('deny') is not None:
        # If the user denied consent, delete the session and acknowledge.
        try:
            gateway.delete_session(session_id)
        except requests.HTTPError as e:
            if e.response is not None and e.response.status_code == 404:
                # Login session has expired or cannot be found, given that we're deleting it
                # we don't display an error.
                LOG.warning('Attempted to delete a session which could not be found')
            else:
                raise e
        return render(request, 'login/deniedconsent.html')
    elif request.POST.get('allow') is not None:
        # If the user gave consent, tell the Gateway the session can proceed and redirect back
        # to the application.
        try:
            url = gateway.proceed_with_session(session_id, filtered_scopes, user_claims)
            return HttpResponse(status=302, headers={'Location': url})
        except requests.HTTPError as e:
            if e.response is not None and e.response.status_code == 404:
                # Login session has expired or cannot be found
                LOG.warning('Attempted to delete a session which could not be found')
                # we assume that this is due to the session expiring in the API Gateway
                return render(request, 'login/sessiontimeout.html', status=400)
            raise e

    # If we get here, the user somehow did not deny or allow consent.
    LOG.error('User did not allow or deny consent: %r', request.POST)
    return _render_400(request)


def _render_400(request):
    return render(request, '400.html', status=400)


def _render_403(request):
    return render(request, '403.html', status=403)


def _render_500(request):
    return render(request, '500.html', status=500)


def _base_url(url):
    """
    Return a URL with path, query and fragment stripped.

    """
    parsed = urllib.parse.urlparse(url)
    parsed = parsed._replace(path='/')
    parsed = parsed._replace(params='')
    parsed = parsed._replace(query='')
    parsed = parsed._replace(fragment='')
    return parsed.geturl()


def _get_lookup_connection():
    """
    Return a lookup connection, appropriately authenticated if necessary.

    """
    conn = ibisclient.connection.createConnection()
    conn.set_username(getattr(settings, 'UCAMLOOKUP_USERNAME', None))
    conn.set_password(getattr(settings, 'UCAMLOOKUP_PASSWORD', None))
    return conn
