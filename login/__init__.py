"""
This application implements the login and consent UI for the API Gateway.
End-user login for the Gateway is de-coupled from the Apigee configuration
itself by means of the `login and consent
API
<https://gitlab.developers.cam.ac.uk/uis/devops/api/gateway-ops/-/blob/master/doc/oauth2-login-api/openapi.yaml>`_.

Required settings
-----------------

The following settings are *required*:

* **GATEWAY_CLIENT_ID** Client id of login and consent application as registered with the API
    Gateway.
* **GATEWAY_CLIENT_SECRET** Client secret of login and consent application as registered with the
    API Gateway.
* **GATEWAY_JWT_AUDIENCE** Expected audience claim for signed session JWT from the API Gateway.
* **GATEWAY_OPENID_CONFIGURATION_URL** URL pointing to the OpenID discovery document for the API
    Gateway.
* **GATEWAY_BASE_URL** Base URL for forming API gateway requests. Usually
    `https://api.apps.cam.ac.uk/`

Optional settings
-----------------

The following settings are *optional*:

* **GATEWAY_LOGIN_SCOPES** List of scopes to request when authenticating to the API Gateway in
    order to process logins. Defaults to `https://api.apps.cam.ac.uk/oauth2/logins` and
    `https://api.apps.cam.ac.uk/gworkspace/users.readonly`.

"""
default_app_config = 'login.apps.Config'
