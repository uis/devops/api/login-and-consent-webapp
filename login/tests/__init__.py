import json
import unittest.mock as mock

import faker
import jwcrypto.jwk
import jwcrypto.jwt
import requests

from django.conf import settings


class MockOpenIDConfigurationMixin:
    """
    Mixin which mocks the OIDC discovery process used by login.gateway.

    """

    def set_up_oidc_config_mock(self):
        """
        Install mocks and register cleanup handlers.

        Initialises:

            self.faker (if not present)
            self.key - JWK signing key
            self.keyset - JWKS for issuer
            self.oauth2_base_url - Base URL for OAuth API
            self.oidc_configuration - OIDC discovery document returned by OIDC configuration
                endpoint
            self.cached_session_mock - magic mock representing the cacheing requests.Session
                used to fetch OIDC configuration and JWKS keys

        The get() method of self.cached_session_mock is mocked to return appropriate mock responses
        for the OIDC URLs.

        """
        if not hasattr(self, 'faker'):
            self.faker = faker.Faker()

        # Construct a key id, key and keyset
        self.key = jwcrypto.jwk.JWK.generate(
            kty='RSA', size=2048, kid=self.faker.password(special_chars=False))
        self.keyset = jwcrypto.jwk.JWKSet()
        self.keyset['keys'].add(self.key)

        # Construct a mock discovery document.
        self.oauth2_base_url = f'{settings.GATEWAY_BASE_URL}oauth2/v1/'
        self.oidc_configuration = {
            'issuer': self.oauth2_base_url.rstrip('/'),
            'authorization_endpoint': f'{self.oauth2_base_url}auth',
            'token_endpoint': f'{self.oauth2_base_url}token',
            'userinfo_endpoint': f'{self.oauth2_base_url}userinfo',
            'jwks_uri': f'{self.oauth2_base_url}.well-known/jwks.json',
            'response_types_supported': ['code'],
            'response_modes_supported': ['query', 'fragment'],
            'grant_types_supported': ['client_credentials', 'authorization_code'],
            'id_token_signing_alg_values_supported': ['RS256'],
            'token_endpoint_auth_methods_supported': ['client_secret_post', 'client_secret_basic'],
            'code_challenge_methods_supported': ['S256']
        }

        # Mock login.gateway._get_cached_requests_session
        get_cached_session_patcher = mock.patch('requests_cache.CachedSession')
        self.addCleanup(get_cached_session_patcher.stop)
        get_cached_session_mock = get_cached_session_patcher.start()
        self.cached_session_mock = get_cached_session_mock.return_value

        # Mock side effect for requests.get() for the various URLs fetched as part of the token
        # verification dance.
        get_method_return_values = {url: mock.MagicMock() for url in [
            self.oidc_configuration['jwks_uri'],
            settings.GATEWAY_OPENID_CONFIGURATION_URL,
        ]}
        not_found_mock = mock.MagicMock()
        not_found_mock.raise_for_status.side_effect = requests.HTTPError

        rv = get_method_return_values[self.oidc_configuration['jwks_uri']]
        rv.content = self.keyset.export(private_keys=False)
        rv.json.return_value = json.loads(rv.content)

        rv = get_method_return_values[settings.GATEWAY_OPENID_CONFIGURATION_URL]
        rv.content = json.dumps(self.oidc_configuration)
        rv.json.return_value = self.oidc_configuration

        self.cached_session_mock.get.side_effect = (
            lambda url, *args, **kwags: get_method_return_values.get(url, not_found_mock))
