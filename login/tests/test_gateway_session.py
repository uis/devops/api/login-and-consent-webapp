import unittest.mock as mock

from django.conf import settings
from django.test import TestCase, override_settings

from .. import gateway

from . import MockOpenIDConfigurationMixin


@override_settings(
    GATEWAY_CLIENT_ID='placeholder-id',
    GATEWAY_CLIENT_SECRET='placeholder-secret',
)
class TestAPIGatewaySession(MockOpenIDConfigurationMixin, TestCase):
    def setUp(self):
        self.set_up_oidc_config_mock()

    def test_get_api_gateway_session(self):
        "API gateway session is appropriately authenticated."
        with mock.patch('requests_auth.OAuth2ClientCredentials') as mock_auth:
            session = gateway._get_api_gateway_session()
            self.assertIs(session.auth, mock_auth.return_value)
        mock_auth.assert_called_with(
            token_url=self.oidc_configuration['token_endpoint'],
            client_id=settings.GATEWAY_CLIENT_ID,
            client_secret=settings.GATEWAY_CLIENT_SECRET,
            scope=' '.join(settings.GATEWAY_LOGIN_SCOPES),
        )
