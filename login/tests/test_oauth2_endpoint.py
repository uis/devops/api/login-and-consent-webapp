from django.test import TestCase

from .. import gateway

from . import MockOpenIDConfigurationMixin


class TestOAuth2Endpoint(MockOpenIDConfigurationMixin, TestCase):
    def setUp(self):
        self.set_up_oidc_config_mock()

    def test_oauth2_endpoint(self):
        self.assertEqual(
            gateway._oauth2_endpoint('login/12345'),
            f'{self.oauth2_base_url}login/12345'
        )
