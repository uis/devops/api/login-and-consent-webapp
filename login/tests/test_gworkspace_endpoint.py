from django.conf import settings
from django.test import TestCase

from .. import gateway


class TestGoogleWorkspaceEndpoint(TestCase):
    def test_google_workspace_endpoint(self):
        self.assertEqual(
            gateway._google_workspace_endpoint('users/abc@example.com'),
            f'{settings.GATEWAY_BASE_URL}gworkspace/v1/users/abc@example.com'
        )
