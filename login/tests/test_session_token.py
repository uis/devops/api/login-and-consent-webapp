import time

import jwcrypto.jwk
import jwcrypto.jwt

from django.test import TestCase

from .. import gateway

from . import MockOpenIDConfigurationMixin


class TestDecodeSessionToken(MockOpenIDConfigurationMixin, TestCase):
    def setUp(self):
        self.set_up_oidc_config_mock()

        # Expected audience
        self.expected_audience = 'https://login.example.invalid/'

        # Create a mock session id
        self.session_id = ''.join(self.faker.random_letters(10))

    def test_basic_verification(self):
        "A valid token is decoded to the session id."
        self.assertEqual(
            self.session_id,
            gateway.decode_session_token(self.make_session_token(), self.expected_audience)
        )

    def test_sub_required(self):
        "An sub claim is required."
        with self.assertRaises(jwcrypto.jwt.JWTMissingClaim):
            gateway.decode_session_token(
                self.make_session_token(extra_claims={'sub': None}), self.expected_audience)

    def test_issuer_required(self):
        "An issuer claim is required."
        with self.assertRaises(jwcrypto.jwt.JWTMissingClaim):
            gateway.decode_session_token(
                self.make_session_token(extra_claims={'iss': None}), self.expected_audience)

    def test_wrong_issuer(self):
        "The issuer claim must be correct."
        with self.assertRaises(jwcrypto.jwt.JWTInvalidClaimValue):
            gateway.decode_session_token(self.make_session_token(extra_claims={
                'iss': 'https://some-other.issuer.example.invalid/'
            }), self.expected_audience)

    def test_audience_required(self):
        "An audience claim is required."
        with self.assertRaises(jwcrypto.jwt.JWTMissingClaim):
            gateway.decode_session_token(
                self.make_session_token(extra_claims={'aud': None}), self.expected_audience)

    def test_wrong_audience(self):
        "The audience claim must be correct."
        with self.assertRaises(jwcrypto.jwt.JWTInvalidClaimValue):
            gateway.decode_session_token(self.make_session_token(extra_claims={
                'aud': 'https://some-other.audience.example.invalid/'
            }), self.expected_audience)

    def test_expiry_required(self):
        "An expiry claim is required."
        with self.assertRaises(jwcrypto.jwt.JWTMissingClaim):
            gateway.decode_session_token(
                self.make_session_token(extra_claims={'exp': None}), self.expected_audience)

    def test_expired_token(self):
        "The token must not have expired"
        with self.assertRaises(jwcrypto.jwt.JWTExpired):
            gateway.decode_session_token(
                self.make_session_token(extra_claims={'exp': 10}), self.expected_audience)

    def test_wrong_key(self):
        "The token must be signed by the correct key"
        key = jwcrypto.jwk.JWK.generate(
            kty='RSA', size=2048, kid=self.faker.password(special_chars=False))
        with self.assertRaises(jwcrypto.jws.JWKeyNotFound):
            gateway.decode_session_token(self.make_session_token(key=key), self.expected_audience)

    def test_jwks_fetched(self):
        "The JWKS has been fetched."
        gateway.decode_session_token(self.make_session_token(), self.expected_audience)
        self.cached_session_mock.get.assert_called_with(self.oidc_configuration['jwks_uri'])

    def test_jwks_status_checked(self):
        "The JWKS response had its status checked."
        gateway.decode_session_token(self.make_session_token(), self.expected_audience)
        response = self.cached_session_mock.get(self.oidc_configuration['jwks_uri'])
        response.raise_for_status.assert_called()

    def make_session_token(
            self, *, extra_header=None, extra_claims=None, key=None):
        """
        Make a session token. Allows signing key, header and claims to be overridden. Anything
        which ends up in the header/claims set to None is removed.

        """
        key = key if key is not None else self.key
        extra_header = extra_header if extra_header is not None else {}
        extra_claims = extra_claims if extra_claims is not None else {}

        header = {
            'alg': 'RS256',
            'kid': self.key.key_id,
        }
        header.update(extra_header)
        for k, v in list(header.items()):
            if v is None:
                del header[k]

        claims = {
            'sub': self.session_id,
            'exp': int(time.time() + 3600),
            'iss': self.oidc_configuration['issuer'],
            'aud': self.expected_audience,
        }
        claims.update(extra_claims)
        for k, v in list(claims.items()):
            if v is None:
                del claims[k]

        token = jwcrypto.jwt.JWT(header=header, claims=claims)
        token.make_signed_token(key)
        return token.serialize()
