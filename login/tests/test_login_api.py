import unittest.mock as mock

from django.test import TestCase

from .. import gateway

from . import MockOpenIDConfigurationMixin


class MockAPIGatewaySessionMixin:
    def set_up_get_api_session_mock(self):
        patcher = mock.patch('login.gateway._get_api_gateway_session')
        mocked = patcher.start()
        self.addCleanup(patcher.stop)
        self.api_gateway_session = mocked.return_value


class TestGetSession(MockAPIGatewaySessionMixin, MockOpenIDConfigurationMixin, TestCase):
    def setUp(self):
        self.set_up_oidc_config_mock()
        self.set_up_get_api_session_mock()
        self.api_gateway_session.get.return_value.json.return_value = {
            'app_name': 'Test app',
            'redirect_uri': 'https://some-redirect.invalid/',
            'req_scope': '',
            'unknown_properties': 'are_ignored',
        }

    def test_get_session(self):
        "Getting a session GET-s the correct endpoint and checks status."
        session = gateway.get_session('12345')
        self.api_gateway_session.get.assert_called_with(f'{self.oauth2_base_url}login/12345')
        resp = self.api_gateway_session.get.return_value
        resp.raise_for_status.assert_called()
        self.assertEqual(session.app_name, 'Test app')
        self.assertEqual(session.redirect_uri, 'https://some-redirect.invalid/')
        self.assertEqual(session.req_scope, '')


class TestDeleteSession(MockAPIGatewaySessionMixin, MockOpenIDConfigurationMixin, TestCase):
    def setUp(self):
        self.set_up_oidc_config_mock()
        self.set_up_get_api_session_mock()

    def test_delete_session(self):
        "Invalidating a session DELETE-s the correct endpoint and checks status."
        gateway.delete_session('12345')
        self.api_gateway_session.delete.assert_called_with(f'{self.oauth2_base_url}login/12345')
        resp = self.api_gateway_session.delete.return_value
        resp.raise_for_status.assert_called()


class TestProceedWithSession(MockAPIGatewaySessionMixin, MockOpenIDConfigurationMixin, TestCase):
    def setUp(self):
        self.set_up_oidc_config_mock()
        self.set_up_get_api_session_mock()
        self.api_gateway_session.post.return_value.is_redirect = True
        self.api_gateway_session.post.return_value.headers = {'location': 'https://example.com/'}

    def test_proceed_with_session(self):
        "Proceeding with a session POSTs user claims and passes allowed scopes in query."
        url = gateway.proceed_with_session('123456', scope='one two', user_claims={
            'sub': 'spqr1@v1.person.identifiers.cam.ac.uk',
        })
        self.api_gateway_session.post.assert_called_with(
            f'{self.oauth2_base_url}login/123456/proceed?scope=one+two',
            json={'sub': 'spqr1@v1.person.identifiers.cam.ac.uk'},
            allow_redirects=False
        )
        resp = self.api_gateway_session.post.return_value
        resp.raise_for_status.assert_called()
        self.assertEqual(url, 'https://example.com/')

    def test_proceed_without_redirect(self):
        "Proceeding with a session raises APIError if the Gateway does not redirect."
        self.api_gateway_session.post.return_value.is_redirect = False
        with self.assertRaises(gateway.APIError):
            gateway.proceed_with_session('123456', scope='one two', user_claims={
                'sub': 'spqr1@v1.person.identifiers.cam.ac.uk',
            })
