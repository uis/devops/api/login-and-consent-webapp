import json
import unittest.mock as mock
import importlib

from requests import HTTPError, Response

import faker
import jwcrypto.common
import requests
from ibisclient.dto import IbisPerson, IbisResult
from jwcrypto.jwt import JWTExpired

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from django.urls import reverse

from .. import gateway


MOCK_SCOPE_DESCRIPTIONS = [
    {
        "title": "Friend face",
        "description": "Multi-billion dollar social network",
        "scopes": {
            "friends": "add and remove friends",
            "friends.readonly": "know your friends list",
         },
    },
    {
        "title": "Goggle minus",
        "description": "Discount social network",
        "scopes": {
            "circles": "create, read, modify and delete your circles",
            "circles.readonly": "know your circles",
            "posts": "create, read, modify and delete posts",
         },
    },
]


@override_settings(SCOPE_DESCRIPTIONS=MOCK_SCOPE_DESCRIPTIONS)
class TestInitiateView(TestCase):
    def setUp(self):
        self.faker = faker.Faker()

        # Create a fake @cam.ac.uk user and sign them in via Google.
        self.user = get_user_model().objects.create(
            username=self.faker.user_name(), email=f'{self.faker.user_name()}@cam.ac.uk')
        self.client.force_login(self.user, 'social_core.backends.google.GoogleOAuth2')

        # Mock the token validation to always succeed.
        patcher = mock.patch('login.gateway.decode_session_token')
        self.session_id = self.faker.password(special_chars=False)
        self.decode_session_token_mock = patcher.start()
        self.addCleanup(patcher.stop)
        self.decode_session_token_mock.return_value = self.session_id

        # Mock the get_session() function to return mock session info.
        patcher = mock.patch('login.gateway.get_session')
        self.get_session_mock = patcher.start()
        self.addCleanup(patcher.stop)
        self.get_session_mock.return_value = gateway.SessionInfo(
            app_name='Test app',
            req_scope='openid profile email not-a-scope circles.readonly friends posts',
            redirect_uri='https://example.com/')

        # Mock the ibis client
        patcher = mock.patch('ibisclient.connection.createConnection')
        self.ibis_connection = patcher.start().return_value
        self.addCleanup(patcher.stop)
        result = IbisResult()
        self.ibis_connection.invoke_method.return_value = result
        result.person = IbisPerson({'displayName': 'Jo Example'})

    def test_allowed_methods(self):
        """
        Requests other than GET return HTTP 405.

        """
        response = self.client.post(reverse('login-initiate'))
        self.assertEqual(response.status_code, 405)
        response = self.client.put(reverse('login-initiate'))
        self.assertEqual(response.status_code, 405)
        response = self.client.delete(reverse('login-initiate'))
        self.assertEqual(response.status_code, 405)

    def test_login_required(self):
        """
        GET-ing the initiate view with no user signed in redirects to login.

        """
        self.client.logout()
        response = self.get()
        self.assertEqual(response.status_code, 302)

    def test_cam_ac_uk_user_required(self):
        """
        The user must have a cam.ac.uk email address.

        """
        non_cam_user = get_user_model().objects.create(
            username=self.faker.user_name(), email=f'{self.faker.user_name()}@uis.cam.ac.uk')
        self.client.force_login(non_cam_user, 'social_core.backends.google.GoogleOAuth2')
        response = self.get()
        self.assertEqual(response.status_code, 403)

    def test_token_required(self):
        """
        GET-ing the initiate view with no token returns a Bad Request response.

        """
        response = self.get(with_token=False)
        self.assertEqual(response.status_code, 400)

    def test_valid_token_required(self):
        """
        GET-ing the initiate view with an invalid token returns a Bad Request response.

        """
        self.decode_session_token_mock.side_effect = jwcrypto.common.JWException
        response = self.get()
        self.assertEqual(response.status_code, 400)

    @override_settings(
        GATEWAY_JWT_AUDIENCE='https://audience.example.invalid/'
    )
    def test_token_is_validated(self):
        """
        The token passed to the view is validated.

        """
        response = self.get()
        self.decode_session_token_mock.assert_called_with(
            'mock-token', 'https://audience.example.invalid/')
        self.assertEqual(response.status_code, 200)

    def test_session_retrieved(self):
        """
        The session information is retrieved from the gateway.

        """
        response = self.get()
        self.get_session_mock.assert_called_with(self.session_id)
        self.assertEqual(response.status_code, 200)

    def test_session_expired(self):
        """
        If session expired between token verification and getting the info,
        return a bad request response.

        """
        response = mock.MagicMock()
        response.status_code = 404
        self.get_session_mock.side_effect = requests.HTTPError(response=response)
        response = self.get()
        self.assertEqual(response.status_code, 400)
        self.assertTemplateUsed(response, 'login/sessiontimeout.html')

    def test_session_http_error(self):
        """
        If there was some other HTTP error getting the session info, return a 500 response.
        Return a bad request response.

        """
        response = mock.MagicMock()
        response.status_code = 500
        self.get_session_mock.side_effect = requests.HTTPError(response=response)
        response = self.get()
        self.assertEqual(response.status_code, 500)

    def test_jwt_expiry(self):
        """
        If the jwt passed in the header has expired, we should get a 400 response indicating
        that the session has expired.

        """
        self.decode_session_token_mock.side_effect = JWTExpired()

        response = self.get()
        self.assertEqual(response.status_code, 400)
        self.assertTemplateUsed(response, 'login/sessiontimeout.html')

    def test_sets_session(self):
        """
        The view sets expected session parameters.

        """
        self.get()
        session = self.client.session
        self.assertEqual(session['session_id'], self.session_id)
        self.assertEqual(
            set(session['filtered_scopes'].split(' ')),
            {'email', 'openid', 'profile', 'circles.readonly', 'friends', 'posts'}
        )
        self.assertIsNotNone(session['user_claims_json'])

    @override_settings(
        UCAMLOOKUP_USERNAME='lookup-user',
        UCAMLOOKUP_PASSWORD='lookup-password',
    )
    def test_sets_lookup_credentials(self):
        """
        Lookup credentials are taken from settings.

        """
        self.get()
        self.ibis_connection.set_username.assert_called_with('lookup-user')
        self.ibis_connection.set_password.assert_called_with('lookup-password')

    def test_uses_template(self):
        """
        The view renders the expected template.

        """
        self.assertTemplateUsed(self.get(), 'login/consent.html')

    def get(self, with_token=True):
        """
        GET the view, optionally passing a mock token.

        """
        if with_token:
            url = f"{reverse('login-initiate')}?token=mock-token"
        else:
            url = reverse('login-initiate')
        return self.client.get(url, HTTP_HOST='login.example.invalid')


class TestConsentView(TestCase):
    def setUp(self):
        self.faker = faker.Faker()

        # Create a fake @cam.ac.uk user and sign them in via Google.
        self.user = get_user_model().objects.create(
            username=self.faker.user_name(), email=f'{self.faker.user_name()}@cam.ac.uk')
        self.client.force_login(self.user, 'social_core.backends.google.GoogleOAuth2')
        crsid = self.user.email.split('@')[0]

        # Add a fake session from the initiate view.
        self.session = self.get_session()
        self.session_id = self.faker.password(special_chars=False)
        self.session['session_id'] = self.session_id
        self.session['filtered_scopes'] = 'openid email'
        self.session['user_claims_json'] = json.dumps({
            'sub': f'{crsid}@v1.person.identifiers.cam.ac.uk',
            'email': self.user.email,
        })
        self.session.save()
        self.set_session_cookies(self.session)

        # Mock the delete_session function.
        patcher = mock.patch('login.gateway.delete_session')
        self.delete_session_mock = patcher.start()
        self.addCleanup(patcher.stop)

        # Mock the proceed_with_session function.
        patcher = mock.patch('login.gateway.proceed_with_session')
        self.proceed_with_session = patcher.start()
        self.addCleanup(patcher.stop)
        self.app_redirect_url = 'https://application.example.invalid/redirect'
        self.proceed_with_session.return_value = self.app_redirect_url

    def test_allowed_methods(self):
        """
        Requests other than POST return HTTP 405.

        """
        response = self.client.get(reverse('login-consent'))
        self.assertEqual(response.status_code, 405)
        response = self.client.put(reverse('login-consent'))
        self.assertEqual(response.status_code, 405)
        response = self.client.delete(reverse('login-consent'))
        self.assertEqual(response.status_code, 405)

    def test_session_id_required(self):
        """
        Session must have session_id.

        """
        del self.session['session_id']
        self.session.save()
        self.set_session_cookies(self.session)
        self.assertEqual(self.post().status_code, 500)

    def test_filtered_scopes_required(self):
        """
        Session must have filtered scope list.

        """
        del self.session['filtered_scopes']
        self.session.save()
        self.set_session_cookies(self.session)
        self.assertEqual(self.post().status_code, 500)

    def test_user_claims_required(self):
        """
        Session must have user claims.

        """
        del self.session['user_claims_json']
        self.session.save()
        self.set_session_cookies(self.session)
        self.assertEqual(self.post().status_code, 500)

    def test_mismatched_user(self):
        """
        If the signed in user doesn't match the one from state, the request is invalid.

        """
        self.user.email = 'somethingelse@cam.ac.uk'
        self.user.save()
        response = self.post()
        self.assertEqual(response.status_code, 400)

    def test_no_decision(self):
        """
        Not allowing *or* denying fails with a Bad Request.

        """
        response = self.post({})
        self.assertEqual(response.status_code, 400)

    def test_deny(self):
        """
        Denying consent calls delete_session.

        """
        response = self.post({'deny': 'some-value'})
        self.assertEqual(response.status_code, 200)
        self.delete_session_mock.assert_called_with(self.session_id)

    def test_deny_succeeds_if_session_not_found(self):
        """
        Denying consent proceeds even if session does not exist.

        """
        # mock a 404 from `delete_session_mock`
        self.delete_session_mock.side_effect = self.raises_error_code(404)

        response = self.post({'deny': 'some-value'})
        self.assertEqual(response.status_code, 200)
        self.delete_session_mock.assert_called_with(self.session_id)

    def test_deny_fails_if_delete_session_fails(self):
        """
        Denying consent fails if delete_session fails.

        """
        # mock a bad response from `delete_session_mock`
        self.delete_session_mock.side_effect = self.raises_error_code(502)

        with self.assertRaises(HTTPError):
            self.post({'deny': 'some-value'})

    def test_allow(self):
        """
        Allowing consent calls proceed_with_session

        """
        user_claims = json.loads(self.session['user_claims_json'])
        response = self.post({'allow': 'some-value'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.headers['location'], self.app_redirect_url)
        self.proceed_with_session.assert_called_with(
            self.session_id, self.session['filtered_scopes'], user_claims)

    def test_allow_redirects_if_session_expired(self):
        """
        If `proceed_with_session` fails with a 404 error then we should get the
        session timed out page.

        """
        # mock a 404 from `proceed_with_session`
        self.proceed_with_session.side_effect = self.raises_error_code(404)

        user_claims = json.loads(self.session['user_claims_json'])
        response = self.post({'allow': 'some-value'})

        self.assertEqual(response.status_code, 400)
        self.assertTemplateUsed(response, 'login/sessiontimeout.html')

        self.proceed_with_session.assert_called_with(
            self.session_id, self.session['filtered_scopes'], user_claims)

    def test_allow_fails_if_proceed_with_session(self):
        """
        Denying consent fails if delete_session fails.

        """
        # mock a bad response from `proceed_with_session`
        self.proceed_with_session.side_effect = self.raises_error_code(501)

        with self.assertRaises(HTTPError):
            self.post({'allow': 'some-value'})

    def post(self, data=None):
        """
        POST to the view, optionally passing form data. If no data is provided, a general "allow"
        request is made.

        """
        data = data if data is not None else {'allow': 'Allow'}
        return self.client.post(reverse('login-consent'), data=data)

    def get_session(self):
        if self.client.session:
            session = self.client.session
        else:
            engine = importlib.import_module(settings.SESSION_ENGINE)
            session = engine.SessionStore()
        return session

    def set_session_cookies(self, session):
        # Set the cookie to represent the session
        session_cookie = settings.SESSION_COOKIE_NAME
        self.client.cookies[session_cookie] = session.session_key
        cookie_data = {
            'max-age': None,
            'path': '/',
            'domain': settings.SESSION_COOKIE_DOMAIN,
            'secure': settings.SESSION_COOKIE_SECURE or None,
            'expires': None}
        self.client.cookies[session_cookie].update(cookie_data)

    def raises_error_code(self, error_code):
        def raises(*args, **kwargs):
            not_found_response = Response()
            not_found_response.status_code = error_code
            raise HTTPError(response=not_found_response)
        return raises
