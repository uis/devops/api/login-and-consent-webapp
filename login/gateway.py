"""
API-gateway related operations.

"""
import json
import logging
import urllib.parse

import jwcrypto.common
import jwcrypto.jwk
import jwcrypto.jwt
import pydantic
import requests
import requests_auth
import requests_cache

from django.conf import settings

LOG = logging.getLogger(__name__)


def decode_session_token(unverified_token, expected_audience):
    """
    Decode and verify a login session token.

    Args:
        unverified_token (str): unverified JWT
        expected_audience (str): expected "aud" claim in JWT

    Returns:
        str: The verified session id.

    Raises:
        jwcrypto.common.JWException: token is invalid
        requests.HTTPError: error fetching public keys

    """
    oidc_config = _get_oidc_configuration()
    jwt = jwcrypto.jwt.JWT(
        algs=['RS256'],
        check_claims={
            'iss': oidc_config['issuer'],
            'aud': expected_audience,
            'sub': None,  # == "must be present"
            'exp': None,  # == "must be present"
        }
    )
    jwt.deserialize(unverified_token, key=_get_jwks())
    return json.loads(jwt.claims)['sub']


class SessionInfo(pydantic.BaseModel):
    """
    A subset of the information returned by the /login/{sessionid} endpoint.

    """
    #: Human-friendly name for application requesting login.
    app_name: str

    #: Redirect URI for application.
    redirect_uri: str

    #: Space-separated list of requested scopes.
    req_scope: str


class APIError(RuntimeError):
    """
    Exception raised when there is a problem with a response from the API Gateway.

    """


def get_session(session_id) -> SessionInfo:
    """
    Retrieve an in-flight login session.

    Args:
        session_id (str): Session id as passed from API Gateway.

    Returns:
        SessionInfo: Information about the in-flight session.

    """
    resp = _get_api_gateway_session().get(_oauth2_endpoint(
        f'login/{urllib.parse.quote(session_id)}'))
    resp.raise_for_status()
    return SessionInfo(**(resp.json()))


def delete_session(session_id):
    """
    Delete (invalidate) in-flight login session.

    Args:
        session_id (str): Session id as passed from API Gateway.

    """
    resp = _get_api_gateway_session().delete(_oauth2_endpoint(
        f'login/{urllib.parse.quote(session_id)}'))
    resp.raise_for_status()


def proceed_with_session(session_id, scope, user_claims):
    """
    Approve an in-flight login session to proceed.

    Args:
        session_id (str): Session id as passed from API Gateway.
        scope (str): Space separated list of *approved* scopes.
        user_claims (dict): User claims to associated with id token.

    Raises:
        APIError: the response from the gateway was not a redirect.

    Returns:
        str: the URL to redirect back to the application.

    """
    query = urllib.parse.urlencode({'scope': scope})
    resp = _get_api_gateway_session().post(
        _oauth2_endpoint(f'login/{urllib.parse.quote(session_id)}/proceed?{query}'),
        json=user_claims, allow_redirects=False)
    resp.raise_for_status()
    if not resp.is_redirect:
        LOG.error('Expected redirect from gateway. Got: %r', resp.status_code)
        raise APIError('Expected redirect')
    return resp.headers['location']


def _oauth2_endpoint(path):
    """
    Return full URL to OAuth2 API endpoint given path.

    """
    return urllib.parse.urljoin(settings.GATEWAY_BASE_URL, f'oauth2/v1/{path}')


def _google_workspace_endpoint(path):
    """
    Return full URL to Google Workspace API endpoint given path.

    """
    return urllib.parse.urljoin(settings.GATEWAY_BASE_URL, f'gworkspace/v1/{path}')


def _get_jwks():
    """
    Return a jwcrypto.jwk.JWKSet corresponding to the public gateway keys.

    """
    # Fetch the JWKS as specified in the discovery document and parse.
    r = _get_cached_requests_session().get(_get_oidc_configuration()['jwks_uri'])
    r.raise_for_status()
    return jwcrypto.jwk.JWKSet.from_json(r.content)


def _get_oidc_configuration():
    """
    Return a parsed OpenID discovery document.

    """
    r = _get_cached_requests_session().get(settings.GATEWAY_OPENID_CONFIGURATION_URL)
    r.raise_for_status()
    return r.json()


def _get_api_gateway_session():
    """
    Returns a Requests session authenticated against the API gateway. Auth token is cached so
    it is safe to call this multiple times.

    """
    oidc_configuration = _get_oidc_configuration()
    session = requests.Session()
    # NB: requests_auth performs transparent token cacheing.
    session.auth = requests_auth.OAuth2ClientCredentials(
        token_url=oidc_configuration['token_endpoint'],
        client_id=settings.GATEWAY_CLIENT_ID,
        client_secret=settings.GATEWAY_CLIENT_SECRET,
        scope=' '.join(settings.GATEWAY_LOGIN_SCOPES),
    )
    return session


def _get_cached_requests_session():
    """
    Return a requests.Session object which includes cacheing suitable for use with JWKS and OpenID
    discovery endpoints.

    """
    # Default is to expire after 300s == 5m. We're unlikely to rotate keys faster than that(!)
    return requests_cache.CachedSession(
        backend='memory', namespace='login-jwks', expire_after=300)
