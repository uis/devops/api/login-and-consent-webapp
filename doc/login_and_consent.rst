Login and Consent Application
=============================

.. automodule:: login
    :members:

Installation
````````````

Add the ``login`` application to your
``INSTALLED_APPS`` configuration as usual.

Views and serializers
`````````````````````

.. automodule:: login.views
    :members:

Default URL routing
```````````````````

.. automodule:: login.urls
    :members:


Integration with the API Gateway
````````````````````````````````

.. automodule:: login.gateway
    :members:

Scopes
``````

.. automodule:: login.scopes
    :members:

Application configuration
`````````````````````````

.. automodule:: login.apps
    :members:
