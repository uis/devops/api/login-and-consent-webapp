# Login and consent application for the API Gateway

This application implements the login and consent UI for the API Gateway.
End-user login for the Gateway is de-coupled from the Apigee configuration
itself by means of the [login and consent
API](https://gitlab.developers.cam.ac.uk/uis/devops/api/gateway-ops/-/blob/master/doc/oauth2-login-api/openapi.yaml).

In brief, when the Gateway requires an end-user login, it redirects to this
application passing a signed JWT containing an opaque login session id. This
application authenticates the current user, retrieves information on the
in-flight login session via the login and consent API and gets user consent.
Depending on the user's response the login session is allowed to proceed or is
terminated.

## Developer Quickstart and Testing

To know how to get started with this project visit our
[Guidebook](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/webapp-dev-environment/).

To view notes on how to get the project working locally visit the [getting
started
section](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/webapp-dev-environment/#getting-started-with-a-project)
of the Guidebook. Note that some extra secrets are required. See the relevant
section below.

To get started with running unit tests visit our [testing
section](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/webapp-dev-environment/#running-tests)
of the Guidebook.

### Testing the login flow

The API Gateway operations project contains instructions for [manually testing
the login
flow](https://gitlab.developers.cam.ac.uk/uis/devops/api/gateway-ops/-/blob/master/doc/oauth2-design-and-implementation.md#user-content-manually-testing-the-flows).

Initiating the authorisation code flow via the `/auth` endpoint should redirect
to the login and consent app with a URL of the form
`https://login.example.com/?token=...`. Rewriting this URL to be
`https://localhost:8000/?token=...` lets you test the login flow using the local
application.

## Required settings

See the [login module docstring](login/__init__.py) for a list of required
settings for the login and consent module. In addition you will need to
[configure OAuth2
credentials](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/webapp-dev-environment/#google-oauth2-credentials)
for the application along with Lookup credentials.

The [secrets.env.in](secrets.env.in) file contains a template configuration. For
developers within UIS, a [secrets.env file suitable for use in local
development](https://start.1password.com/open/i?a=D3ATZUD36RDHLDKSVJUQZWGORQ&v=rzdugks5meinz5oc772yudf3ra&i=5kwf5inds3zvae3csnlf7c4lne&h=uis-devops.1password.eu)
is kept in 1password.

## Copyright License

See the [LICENSE](LICENSE) file for details.
